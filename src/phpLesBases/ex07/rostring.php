<?php

$text = $argv;
if ($argc < 2) {
    exit;
}

$texte = explode(' ', $text[1]);
$fruit = array_shift($texte);

$mot = implode(' ', $texte);
$replace = preg_replace('/[[:blank:]]+/', ' ', $mot);

echo $replace . $fruit . "\n";
