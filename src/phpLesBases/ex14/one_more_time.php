<?php

$jour = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];
$mois = ['Janvier' => 1, 'Fevrier' => 2, 'Mars' => 3, 'Avril' => 4, 'Mai' => 5, 'Juin' => 6, 'Juillet' => 7, 'Aout' => 8, 'Septembre' => 9, 'Octobre' => 10, 'Novembre' => 11, 'Decembre' => 12];

$arg = array_slice($argv, 1);

foreach ($arg as $tab) {
    $res = explode(' ', $tab);
    if (!empty($res[0])) {
        $res0 = ucfirst(strtolower($res[0]));
        if ($res0 == in_array($res0, $jour)) {
            if (is_numeric($res[1])) {
                if ($res[1] > 0 && $res[1] < 31) {
                    $res2 = ucfirst(strtolower($res[2]));
                    if ($res2 == array_key_exists($res2, $mois)) {
                        $resheure = explode(':', $res[4]);

                        if ($resheure[0] < 24 && $resheure[1] < 60 && $resheure[2] < 60) {
                            $Chiffrejour = $res[1];
                            $Chiffremois = $mois[$res2];
                            if (checkdate($Chiffremois, $Chiffrejour, $res[3])) {
                                $result = true;
                                array_push($arg, 'heure normale d’Europe centrale');
                                $date = implode(' ', $arg);
                                $parser = new IntlDateFormatter(
                                'fr_FR',
                                IntlDateFormatter::FULL,
                                IntlDateFormatter::FULL
                            );
                                $tsparis = $parser->parse($date);

                                echo $tsparis . "\n";
                            } else {
                                echo "Wrong Format\n";
                            }
                        } else {
                            echo "Wrong Format\n";
                        }
                    } else {
                        echo "Wrong Format\n";
                    }
                } else {
                    echo "Wrong Format\n";
                }
            } else {
                echo "Wrong Format\n";
            }
        } else {
            echo "Wrong Format\n";
        }
    } else {
        exit;
    }
}
