<?php

const TYPE_NUMBER = 'Entrez un nombre: ';

while (true) {
    echo $output ?? TYPE_NUMBER;

    $number = trim(fgets(STDIN));
    $answer = is_numeric($number)
        ? "Le chiffre $number est " . ($number % 2 ? 'Impair' : 'Pair')
        : "'$number' n'est pas un chiffre";
    $output = "$answer\n" . TYPE_NUMBER;
}
